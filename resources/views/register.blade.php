<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="welcome" method="POST">
        <h1>Buat Account Baru!</h1>

        <h3>Sign Up Form</h3>
        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

        <p>First name:</p>
        <input type="text" name="fname" id="fname">

        <p>Last name:</p>
        <input type="text" name="lname" id="lname">

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>

        <p>Nationality</p>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" id="language1" name="language" value="id">
        <label for="language1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language" value="en">
        <label for="language2">English</label><br>
        <input type="checkbox" id="language3" name="language" value="other">
        <label for="language3">Other</label>

        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sing Up">
    </form>
</body>

</html>